#!/usr/bin/env python

import socket
import select
import time
import rospy
from std_msgs.msg import String
import thread
import struct


#
# def callback(data):
#     rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
#
# def listener():
#     # In ROS, nodes are uniquely named. If two nodes with the same
#     # node are launched, the previous one is kicked off. The
#     # anonymous=True flag means that rospy will choose a unique
#     # name for our 'listener' node so that multiple listeners can
#     # run simultaneously.
#     rospy.init_node('listener', anonymous=True)
#     rospy.Subscriber("chatter", String, callback)
#     # spin() simply keeps python from exiting until this node is stopped
#     rospy.spin()
#
# def send_msg(sock, msg):
#     # Prefix each message with a 4-byte length (network byte order)
#     msg = msg.encode('utf-8')
#     msg = struct.pack('>I', len(msg)) + msg
#     sock.sendall(msg)
#
# def recv_msg(sock):
#     # Read message length and unpack it into an integer
#     raw_msglen = recvall(sock, 4)
#     if not raw_msglen:
#         return None
#     msglen = struct.unpack('>I', raw_msglen)[0]
#     # Read the message data
#     return recvall(sock, msglen).decode('utf-8')
#
# def recvall(sock, n):
#     # Helper function to recv n bytes or return None if EOF is hit
#     data = b''
#     while len(data) < n:
#         packet = sock.recv(n - len(data))
#         if not packet:
#             return None
#         data += packet
#     return data
#
#
#
#
#
#
# def client_thread(conn, addr):
#     # sends a message to the client whose user object is conn
#     conn.send("Welcome to this chatroom!")
#
#     while True:
#             try:
#                 message = conn.recv(2048)
#                 if message:
#
#                     """prints the message and address of the
#                     user who just sent the message on the server
#                     terminal"""
#                     print "<" + addr[0] + "> " + message
#
#                     # Calls broadcast function to send message to all
#                     message_to_send = "<" + addr[0] + "> " + message
#                     broadcast(message_to_send, conn)
#
#                 else:
#                     """message may have no content if the connection
#                     is broken, in this case we remove the connection"""
#                     remove(conn)
#
#             except:
#                 continue


class AdHocNode(object):

    def __init__(self, host, port, num_connections, subscriber_topic, publisher_topic):
        self.subscriber = rospy.Subscriber(subscriber_topic, String, self.callback,  queue_size = 3)
        self.publisher = rospy.Publisher(publisher_topic, String)

        self.host = host
        self.port = port
        self.clients = set()

        self.server_socket = self.build_server_socket(max_connections)

    def build_server_socket(self, num_connections):
        '''
        generate the server socket
        connections: number of connections the server can have at once
        '''
        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) #not sure why --> look into this
        servesocket.bind((socket.gethostname(), self.port)) #get machine name and bind as socket hostname
        serversocket.listen(num_connections) #turn into a server node

    def callback(self, data):
        '''
        receive data from external topic in the form of
        typ: type of message
        num: number of times to broadcast
        tcp: 'tcp' or 'udp' for dictating how to broadcast
        data: the actual message
        '''
        raw_data = data.data
        typ, num, tcp, data = raw_data.split('|__|')
        message = self.gen_msg(data, typ)

        self.broadcast(message, tcp)
        if tcp == 'UDP': #rebroadcast message num - 1 times if its an unreliable broadcast
            for n in range(num-1):
                self.broadcast(message, tcp)

    def gen_msg(self, data, typ):
        assert type(data) == str
        msg = msg.encode('utf-8')
        msg = struct.pack('>I', len(msg)) + msg
        return msg

    def broadcast(self, message, TCP_UDP):
        '''
        broadcast message via TCP or UDP to all connections

        message (bytes): message to broadcast
        TCP_UDP (str): 'tcp' or 'udp', dictating protocol used
        '''
        for client in self.clients:
            try:
                client.sendall(message)
            except:
                print('failed to send message to client', client)
                client.close()
                # if the link is broken, we remove the client
                self.clients.remove(client)

    def listen(self):
        '''
        loop for listening for connections from other nodes
        '''
        while True:
            conn, addr = self.serversocket.accept() #listen for connections
            print(addr[0] + " connected")
            self.clients.add(conn) #add to connections
            thread.start_new_thread(client_thread,(conn,addr)) #start new thread to listen to messages from new client

        conn.close()
        self.serversocket.close()

    def my_recv(sock):
        '''
        receive message, reading first 4 bytes to determine length
        and returning remaining data in string form

        sock (socket): socket on which it is receiving data
        '''
        # Read message length and unpack it into an integer
        raw_msglen = recvall(sock, 4)
        if not raw_msglen:
            return None
        msglen = struct.unpack('>I', raw_msglen)[0]
        # Read the message data
        return recvall(sock, msglen).decode('utf-8')

    def recvall(sock, length):
        '''
        Helper function to recv length bytes or return None if EOF is hit

        sock (socket): socket being opperated on
        length (int): length of message'''
        data = b''
        while len(data) < length:
            packet = sock.recv(min(length - len(data), 4096))
            if not packet:
                return None
            data += packet
        return data

    def client_thread(self, sock, addr):
        '''
        method to get messages being broadcasted and to publish them to the topic

        sock (socket): socket used to listen on thread
        addr: address of client
        '''
        # receiving messages from a socket
        sock.send("You're now connected")
        while True:
                try: #why the try case?
                    message = my_recv(sock)
                    if message:
                        """prints the message and address of the
                        user who just sent the message on the server
                        terminal"""
                        print("received <" + addr[0] + "> " + message)
                        # Calls broadcast function to send message to all
                        self.publisher.publish(message)
                        # broadcast(message, conn)
                    else:
                        #message may have no content if the connection
                        #is broken, in this case we remove the connection
                        remove(sock)
                except:
                    continue

def main(args):
     '''
     Initializes and cleans up ros node
     args: sys.argv (so user can terminate node)
     '''
     host = '10.2.1.1'
     port = '1234'

     node = AdHocNode(host, port)
     rospy.init_node('ad_hoc_broadcaster', anonymous=True)
     try:
         rospy.spin()
     except KeyboardInterrupt:
         print('Shutting down broadcaster node')

if __name__ == '__main__':
    main(sys.argv)
