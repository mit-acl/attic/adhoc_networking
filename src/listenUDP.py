#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Bool, Int16
import socket #Do I need to add this to the cmake/dependencies
import random
import os
import sys
import udpMessage #Should this be from udpMessage import maxIDMessage?
import threading
import cPickle as pickle
from adhoc_networking.msg import maxIDMissionSpecs, MissionSentResults, MissionRcvdResults, UdpROSMsg #Should this be CAP?
import time


agentIDstr=os.environ['AGENT_NAME'][-1]
agentIDint = int(agentIDstr)
adhoc=True
simNeighbors = True  #Set this true if you want to enforce neighbors
UDP_IP = "10.10.3.255"
UDP_PORT = 5005
sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)  #Need this to allow broadcast
sock.bind((UDP_IP,UDP_PORT))
sock.setblocking(0) #Don't block
print "UDP target IP:", UDP_IP, "port:", UDP_PORT


delay = 10
duration = 30
allSpeeds = [10,25,50,75,100,150,250,500,750,1000]
numTrials = 4


startTime = time.time()+delay
endTime = startTime + duration

rcvdMsgs = []
nRcvd = 0
missionNum=0
allRawResults = {}
print "Est Time", numTrials*len(allSpeeds)*(duration+delay)/60.0, " Mins"
for j in range(numTrials):
    for speed in allSpeeds:
        rcvdMsgs = []
        while time.time()<startTime:
            pass
        #Starting Mission
        print "Speed", speed
        missionNum+=1
        while time.time()<endTime:
            try:
                (rcvd,addr) = sock.recvfrom(10)
                #print rcvd
                rcvdMsgs += [rcvd]
            except socket.error:
                pass
        emptyBuffercounter = 0
        postMissionCounter = 0
        print "EmptyBuffer"
        while emptyBuffercounter<=20:
            try:
                (rcvd,addr) = sock.recvfrom(10)
                #print rcvd
                postMissionCounter += 1
                emptyBuffercounter = 0
            except socket.error:
                emptyBuffercounter+=1
                pass
        print "Receive Buffer Emptied", postMissionCounter, " Msgs"
        startTime += duration + delay
        endTime = startTime + duration
        if j==0:
            allRawResults[speed] = []
        allRawResults[speed] += [rcvdMsgs]
        print len(rcvdMsgs)


allResults = {k:[] for k in allSpeeds}
missionNum = 0
for j in range(numTrials):
    for speed in allSpeeds:
        missionNum += 1
        mResults = allRawResults[speed][j]
        counts = [sum([1 for m in mResults if int(m[0])==agentID and int(m[1:])==missionNum]) for agentID in range(6)]
        allResults[speed] += counts
        print counts
filename = '/home/pi/results/'+str(agentIDint)+'V4FC_LudpPingResults.p'
pickle.dump(allResults,open(filename,'wb'))
print filename
