#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Bool, Int16
import socket #Do I need to add this to the cmake/dependencies
import random
import os
import sys
import udpMessage #Should this be from udpMessage import maxIDMessage?
import threading
import cPickle as pickle
from adhoc_networking.msg import maxIDMissionSpecs, MissionSentResults, MissionRcvdResults, UdpROSMsg #Should this be CAP?
import time


agentIDstr=os.environ['AGENT_NAME'][-1]
agentIDint = int(agentIDstr)
adhoc=True
simNeighbors = True  #Set this true if you want to enforce neighbors
UDP_IP = "10.10.3.255"
UDP_PORT = 5005
sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)  #Need this to allow broadcast
sock.bind((UDP_IP,UDP_PORT))
print "UDP target IP:", UDP_IP, "port:", UDP_PORT

delay = 5
duration = 30
allSpeeds = [10,100,1000,25,50,250,500]

#allSpeeds= [10,20]
numTrials = 7
msgSize = 32
startTime = time.time()+delay
endTime = startTime + duration
sentMsgs = []
senderSet = [{2,4},{1,4}]
allRawResults = {}
missionNum=0
totalResults = []
print "Est Time", len(senderSet)*numTrials*len(allSpeeds)*(duration+delay)/60.0, " Mins"
print "OR"
print "Est Time", len(senderSet)*numTrials*len(allSpeeds)*(duration+delay)/3600.0, " Hrs"
hiddenResults = {speed: [] for speed in allSpeeds}
exposedResults = {speed: [] for speed in allSpeeds}

for j in range(numTrials):
    for speed in allSpeeds:
        for senders in senderSet:
            if agentIDint in senders:
                print "Sending"
                sock.setblocking(1) #Don't block
                nSent = 0
                while time.time()<startTime:
                    pass
                #Starting Mission
                missionNum+=1
                print "Speed", speed
                timeDelay = 1.0/speed
                while time.time()<endTime:
                    sent = sock.sendto(agentIDstr+str(missionNum).zfill(3)+"0"*(msgSize-12),(UDP_IP,UDP_PORT))
                    #print sent
                    sentMsgs += [sent]
                    nSent += 1
                    time.sleep(timeDelay)
                print nSent
                startTime += duration + delay
                endTime = startTime + duration
                if senders == {2,4}:
                    hiddenResults[speed] += [("S",nSent)]
                else:
                    exposedResults[speed] += [("S",nSent)]
            else:
                print "Receiving"
                sock.setblocking(0) #Don't block
                rcvdMsgs = []
                while time.time()<startTime:
                    pass
                #Starting Mission
                print "Speed", speed
                missionNum+=1
                while time.time()<endTime:
                    try:
                        (rcvd,addr) = sock.recvfrom(msgSize)
                        #print rcvd
                        rcvdMsgs += [rcvd]
                    except socket.error:
                        pass
                emptyBuffercounter = 0
                postMissionCounter = 0
                print "EmptyBuffer"
                while emptyBuffercounter<=20:
                    try:
                        (rcvd,addr) = sock.recvfrom(10)
                        #print rcvd
                        postMissionCounter += 1
                        emptyBuffercounter = 0
                    except socket.error:
                        emptyBuffercounter+=1
                        pass
                print "Receive Buffer Emptied", postMissionCounter, " Msgs"
                startTime += duration + delay
                endTime = startTime + duration
                if senders == {2,4}:
                    hiddenResults[speed] += [("R",sum([1 for m in rcvdMsgs if int(m[0])==4]))]
                else:
                    exposedResults[speed] += [("R",sum([1 for m in rcvdMsgs if int(m[0])==4]))]
                print len(rcvdMsgs)

filename = '/home/pi/results/'+'V10_32HiddenResultsAg'+str(agentIDint)+'.p'
pickle.dump(hiddenResults,open(filename,'wb'))
print filename

filename = '/home/pi/results/'+'V10_32ExposedResultsAg'+str(agentIDint)+'.p'
pickle.dump(exposedResults,open(filename,'wb'))
print filename
