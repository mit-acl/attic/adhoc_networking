#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Bool
import socket #Do I need to add this to the cmake/dependencies
import random
import os
import sys
import udpMessage

#Title: BroadcastSpamNode.py
#Author: Noam Buckman, 1/2017
#Descr:  The mission control will constantly send data the other nodes

def spamBroadcaster():
    missionDonePub = rospy.Publisher('missionStatus', Bool, queue_size=1)
    rospy.init_node('spamBroadcaster', anonymous=True)
    rate = rospy.Rate(10000000) # 10hz
    agentIDstr=os.environ['AGENT_NAME'][-1]
    agentIDint = int(agentIDstr)
    random.seed(agentIDint)

    
    #Setting up the UDP Socket
    UDP_IP = "10.10.3.255"
    UDP_PORT = 5005
    sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)  #Need this to allow broadcast
    print "UDP target IP:", UDP_IP
    print "UDP target port:", UDP_PORT

    #Mission Setup    
    # msgSize = (800-22)
    # numOfMsgs = 500
    # numOfAgents = 9
    
    #Load mission parameters
    msgSize = rospy.get_param("/msgSize",800)
    numOfMsgs = rospy.get_param("/numMsgs",500)
    p = rospy.get_param("pSend",1.0)
    activeAgents = rospy.get_param("/activeSenders",[])
    numOfAgents = rospy.get_param("/numAgents",9)
    numTermMsgs = rospy.get_param("/numTermMsgs",1000)


    #Have the agents randomly start at different times (asynchrony)
    rospy.sleep(1)
    rospy.sleep(random.random())
    startTime = rospy.get_time()

    #Create Initial Test Message
    connectionTest = udpMessage.ConnectionTest()
    connectionTest.sender = agentIDint
    connectionTest.status = 0
    connectionTest.msgNum = 0
    connectionTest.totalNums = numOfMsgs
    connectionTest.sleepTime = 200
    connectionTest.msgLength = msgSize
    #ctUDPMsg = connectionTest.toUDPMessage()  #Converts it to a UDP Msg

    missionDone = False
    msgCounter = 0    
    amountSent = 0      

    while not rospy.is_shutdown() and not missionDone:
        connectionTest.msgNum=msgCounter
        ctUDPMsg = connectionTest.toUDPMessage()         

        sizeSent = ctUDPMsg.sendOnUDP(sock,UDP_IP,UDP_PORT)
        amountSent+=sizeSent        
        msgCounter+=1

        if msgCounter == numOfMsgs:         #Done Transmitting Test Msgs
            connectionTest.status = 9
            ctUDPMsg = connectionTest.toUDPMessage()
            finalTime = rospy.get_time()

        rate.sleep()

if __name__ == '__main__':
    try:
        spamBroadcaster()
    except rospy.ROSInterruptException:
        pass
