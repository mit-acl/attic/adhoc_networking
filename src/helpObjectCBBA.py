import copy
from udpMessage import *
import time
class CBBAManager:
    def __init__(self, agentID, robotPose, L_maxTasks=4,pathValueF=None,nTasks=30,nAgents=3):
        #Provide:  max #of tasks for agent
        #          Function to calculate pathValue
        self.agentID = agentID
        self.pose = robotPose
        self.L_maxTasks = L_maxTasks

        if pathValueF == None:
            self.pathValueF = self.defaultValueF
        else:
            self.pathValueF = pathValueF

        self.tasksList = None
        self.nTasks = nTasks
        self.nAgents = nAgents

        self.b_bundle = [] #Ordered by which added first
        self.p_path = [] #Ordered by location in assignment
        self.t_times = [0 for i in range(self.nAgents)]
        self.z_winagents = [0 for i in range(self.nTasks)] #Dict of tasks and the highest bidder so far (agent)
        self.y_winbids = [0 for i in range(self.nTasks)]   #Dict of tasks and the highest bids so far
        self.s_timestamps = [0 for i in range(self.nAgents+1)] #Init time stamp 0
        self.listOfIncomingBundles = []
        self.earliest_new_task_n = 99999

    def defaultValueF(self,path,tasksList):
        #Value function is only based on distance TODO
        value = 0
        t=0
        discount_factor = 0.95
        totalDist = 0
        previous_pose = self.pose
        for j in path:
            task = tasksList[j]
            totalDist = totalDist + previous_pose.dist(task.pose)
            value = value + task.reward * (discount_factor ** totalDist)
            previous_pose = task.pose
        return value

    def initializeLists(self,taskArray):
        self.tasksList = taskArray
        self.nTasks = len(taskArray)
        self.z_winagents = [0 for i in range(self.nTasks)] #Dict of tasks and the highest bidder so far (agent)
        self.y_winbids = [0 for i in range(self.nTasks)]   #Dict of tasks and the highest bids so far
        self.s_timestamps = [0 for i in range(self.nAgents+1)] #Init time stamp 0

    def phase1BuildBundle(self,simTasksSet):
        newTasksSet = set(range(len(self.tasksList)))
        while len(self.b_bundle)< self.L_maxTasks and len(newTasksSet)>0:
            #While there are new tasks available and agent can take more tasks
            J = None #Winning task (J)
            Jvalue = 0 #Winning task marg value
            Jn = 0 #Winning task placement
            for j in newTasksSet:
                j_task = self.tasksList[j]
                if j in self.b_bundle:
                    #print j_task, "in bundle"
                    continue
                #Bid on all the new tasks and get the highest marginal score
                [c_ij, task_n] = self.max_marginal_value(j) #7
                #If task has highest marg value and largest on team, add task to current path
                #print j_task, task_margValue
                if c_ij>self.y_winbids[j]:
                    h_ij = c_ij
                elif abs(c_ij-self.y_winbids[j])<0.000001 and self.agentID > self.z_winagents[j]:
                    h_ij = c_ij
                else:
                    h_ij = 0
                if h_ij>=Jvalue:
                    J = j
                    Jvalue = h_ij
                    Jn = task_n #10

            self.b_bundle.append(J)#11
            self.p_path.insert(Jn,J) #12
            self.y_winbids[J] = Jvalue #13
            i = self.agentID
            self.z_winagents[J] = i
            newTasksSet.remove(J) #Delete the task from "new tasks" since its assigned
        # print "Ph1Ag"+str(self.agentID)+"P", self.p_path
        # print "Ph1Ag"+str(self.agentID)+"B", self.b_bundle

    def max_marginal_value(self,j):
        max_n = 0
        max_value = 0
        for n in range(len(self.p_path)+1):
            new_path = copy.deepcopy(self.p_path) #<- Is there a better way to do this?
            new_path.insert(n,j)
            S_new_value = self.pathValueF(new_path,self.tasksList)
            if S_new_value>max_value:
                max_n = n
                max_value = S_new_value
        return (max_value-self.pathValueF(self.p_path,self.tasksList),max_n)

#     def phase2ConflictResolution(self,printInfo=False):
#         bundle = self.listOfIncomingBundles.pop()
#         self.s_timestamps = self.update_time_array(bundle)
#         print "Entering Phase II"
# #         print "My info (Agent", self.agentID, ")"
# #         print [(i,'%0.2f' % self.y_winbids[i],self.z_winagents[i]) for i in range(len(self.y_winbids))]
# #         print "Their info (Agent", bundle.agentID, ")"
# #         print [(i,'%0.2f' % bundle.y_winbids[i],bundle.z_winagents[i]) for i in range(len(bundle.y_winbids))]
#         self.makeDecision(bundle)
#         # print "Ph2Ag"+str(self.agentID)+"P", self.p_path
#         # print "Ph2Ag"+str(self.agentID)+"B", self.b_bundle
#         cbbaUDPMsg = self.makeCBBAMsg()
#         global outGoingQueue
#         outGoingQueue += [cbbaUDPMsg]

    def phase2ConflictResolution(self,bundle,printTasks=True):
        #This is taken straight from Choi '09 Table 1
        self.min_newTaskBundle_jn = 99999
        i=self.agentID
        k=bundle.agentID
        if printTasks:
            print "j, k, i z_kj, z_ij, y_kj, y_ij"
        for j in range(self.nTasks): ##
            update=False
            # Set these vars z_kj, z_ij, s_km, s_im, y_kj, y_km
            z_ij = self.z_winagents[j]
            z_kj = bundle.z_winagents[j]

            y_ij = self.y_winbids[j]
            y_kj = bundle.y_winbids[j]

            m=z_ij
            s_im = self.s_timestamps[m]
            s_km = bundle.s_timestamps[m]

            if printTasks and j==12:
                print j, k, i, z_kj, z_ij, y_kj, y_ij, s_im, s_km
            #Action rules after communicating with agent j
            if z_kj==k:
                if z_ij==i:
                    if (y_kj>y_ij or (abs(y_kj-y_ij)<0.00001 and k>j)):
                        self.update(j, y_kj,z_kj)
                        update=True
                elif z_ij==k:
                    #Different than the paper
                    self.update(j, y_kj,z_kj)
                    update=True
                    #self.leave()
                elif z_ij==0:
                    self.update(j, y_kj,z_kj)
                    update=True
                else: #M not i,k
                    if (s_km > s_im) or (y_kj>y_ij or (abs(y_kj-y_ij)<0.00001 and k>j)):
                        self.update(j, y_kj,z_kj)
                        update=True
            elif z_kj==i:
                if z_ij==i:
                    self.leave()
                elif z_ij==k:
                    self.reset(j)
                elif z_ij==0:
                    self.leave()
                else: #M not i,k
                    if s_km > s_im:
                        self.reset(j)
            elif z_kj==0:
                if z_ij==i:
                    self.leave()
                elif z_ij==k:
                    self.update(j, y_kj,z_kj)
                    update=True
                elif z_ij==0:
                    self.leave()
                else: #M not i,k
                    m=z_ij
                    if (s_km > s_im):
                        self.update(j, y_kj,z_kj)
                        update=True
            else:
                #Some definitions (override) s_km,s_im, s_kn, s_jn
                m=z_kj
                if m in bundle.s_timestamps:
                    s_km = bundle.s_timestamps[m]
                else:
                    s_km = 0
                if m in self.s_timestamps:
                    s_im = self.s_timestamps[m]
                else:
                    s_im = 0
                n = z_ij
                if n in bundle.s_timestamps:
                    s_kn = bundle.s_timestamps[n]
                else:
                    s_kn = 0
                if n in self.s_timestamps:
                    s_in = self.s_timestamps[n]
                else:
                    s_in = 0

                if z_ij==i:
                    if s_km>s_im and (y_kj>y_ij or (abs(y_kj-y_ij)<0.00001 and k>j)):
                        self.update(j, y_kj,z_kj)
                elif z_ij==k:
                    if s_km>s_im:
                        self.update(j, y_kj,z_kj)
                    else:
                        self.reset(j)
                elif z_ij==m:
                    if s_km>s_im:
                        self.update(j, y_kj,z_kj)
                elif z_ij==0:
                    if s_km>s_im:
                        self.update(j, y_kj,z_kj)
                else:
                    if s_km>s_im and s_kn>s_in:
                        self.update(j, y_kj,z_kj)
                    elif s_km > s_im and (y_kj>y_ij or (abs(y_kj-y_ij)<0.00001 and k>j)):
                        self.update(j, y_kj,z_kj)
                    elif s_kn > s_in and s_im>s_km:
                        self.reset()
        for n in range(len(self.b_bundle)): #(6) pg 916
            if n>self.min_newTaskBundle_jn:
                bn = self.b_bundle[n]
                self.y_winbids[bn] = 0
                self.z_winagents[bn] = 0
        if self.min_newTaskBundle_jn<99999:
            print self.b_bundle[self.min_newTaskBundle_jn], "First Changed"
            print self.b_bundle[self.min_newTaskBundle_jn:], "REMOVED!"
            self.b_bundle = self.b_bundle[:self.min_newTaskBundle_jn]
        setBundle = set(self.b_bundle)
        self.p_path = [n for n in self.p_path if n in setBundle]

    def update_time_array(self,incomingBundle): #Implementing equation (5), pg. 916
        tau_r = time.time() #This may not come here
        k = incomingBundle.agentID
        updated_s_timestamps = [0 for i in self.s_timestamps]

        updated_s_timestamps[k] = tau_r
        updated_s_timestamps[self.agentID] = tau_r #Does this become my own time also?
        for j in range(len(incomingBundle.s_timestamps)):
            if j == k or j==self.agentID:
                pass
            else:
                updated_s_timestamps[j] = max(self.s_timestamps[j],incomingBundle.s_timestamps[j])
        return updated_s_timestamps

    #Part of the decision tree when receiving message
    def update(self,j,y_kj,z_kj):
        self.y_winbids[j] = y_kj
        self.z_winagents[j] = z_kj
        if j in self.b_bundle:
            #This task is in my bundle, #Maybe this should be checked with the dict
            j_n = self.b_bundle.index(j)
            self.min_newTaskBundle_jn = min(j_n,self.min_newTaskBundle_jn)

    def reset(self,j):
        self.y_winbids[j] = 0
        self.z_winagents[j] = 0
        if j in self.b_bundle:
            #This task is in my bundle, I will need to remove this task
            j_n = self.b_bundle.index(j)
            self.min_newTaskBundle_jn = min(j_n,self.min_newTaskBundle_jn)

    def leave(self):
        pass

    def makeCBBAMsg(self,agentID,missionID):
        cbbaMsg = CBBA()
        cbbaMsg.sender = agentID
        cbbaMsg.missionID = missionID #?
        cbbaMsg.msgNum = 1
        cbbaMsg.numTasks = self.nTasks
        cbbaMsg.z_winagents = self.z_winagents
        cbbaMsg.y_winbids = self.y_winbids
        cbbaMsg.s_timestamps = self.s_timestamps
        cbbaUDPMsg = cbbaMsg.toUDPMessage()
        return cbbaUDPMsg


class Pose:
    def __init__(self,x=None,y=None,phi=None,z=None):
        self.X = x
        self.Y = y
        self.Phi = phi

    def dist(self,other):
        return ((self.X-other.X)**2+(self.Y-other.Y)**2)**0.5

    def __str__(self):
        return "X=" + str(self.X) + ", Y=" + str(self.Y)

    def __eq__(self,other):
        return abs(self.X-other.X)<=.1 and abs(self.Y-other.Y)<=0.1

class Task:
    def __init__(self,reward,pose,taskID):
        self.reward = reward
        self.pose = pose
        self.id = taskID

    def __str__(self):
        #return ("Task #" + str(self.id) + " Reward:" + str(self.reward) + '\n' +
                #"Pose:" + "(" + str(self.pose.X) + "," + str(self.pose.Y) + ")" + "\n")
        return str(self.id)

    def __hash__(self):
        return hash(self.id)

    def __eq__(self,other):
        return self.id == other.id

    def __repr__(self):
        return str(self.id)

class Bundle:
    def __init__(self,agentID,y_winbids,z_winagents,s_timestamps):
        self.agentID = agentID
        self.y_winbids = y_winbids
        self.z_winagents = z_winagents
        self.s_timestamps = s_timestamps

    def broadcast(self,sim=None):
        #print "Broadcasted"
        pass
        #sim.broadcast(bundle)

    def __hash__(self):
        return hash(self.agentID)

    def __str__(self):
        return "y: "+str(self.y_winbids) + '\n' + "z: "+str(self.z_winagents) + '\n' + "s:"+str(self.s_timestamps)
