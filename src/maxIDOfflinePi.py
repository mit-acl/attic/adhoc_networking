#!/usr/bin/env python
import rospy
from std_msgs.msg import String, Bool, Int16
import socket #Do I need to add this to the cmake/dependencies
import random
import os
import sys
import udpMessage #Should this be from udpMessage import maxIDMessage?
import threading
import cPickle as pickle
from adhoc_networking.msg import maxIDMissionSpecs, MissionSentResults, MissionRcvdResults, UdpROSMsg #Should this be CAP?
import time
#Title: maxIDOffilnePi.py
#Author: Noam Buckman,5/2017
#Descr:  This node will broadcast and update my running max

def maxIDNode(experimentDirectory):
    rospy.init_node('maxIDNode', anonymous=True)
    print "My Time", time.time()
    rospy.on_shutdown(onShutdownSave)
    global currentMissionID, changingID
    changingID = 0
    #This may need to be moved outside



    #Step 1:  Load the Experiment's List of Mission Specs)

    #entireMissionSpecsFileName = missionSpecsEnteredByUser
    entireMissionSpecsFileName = experimentDirectory + "/experimentSpecs.p"
    entireMissionSpecs = pickle.load(open(entireMissionSpecsFileName,'rb'))
    ################################################3
    random.seed(agentIDint)
    #currentRunningMax = 0 #Reset the currentRunningMax

    #Step 3)  Launch the threads with these settings!
    #Threading
    t = threading.Thread(target=Sending,args=[entireMissionSpecs])
    t.daemon = True
    t.start()
    t2 = threading.Thread(target=Receiving,args=[entireMissionSpecs])
    t2.daemon = True
    t2.start()
    t3 = threading.Thread(target=IDChanging,args=[entireMissionSpecs])
    t3.daemon = True
    t3.start()
    ######################################################
    while experimentRunning and not rospy.is_shutdown():
        rospy.spin()

def Sending(entireMissionSpecs):
    experimentRunning = True
    while experimentRunning and not rospy.is_shutdown():
        for currentMissionSpecs in entireMissionSpecs:
            #1)  Unpack the mission specs
            currentMissionID = currentMissionSpecs.missionID
            activeAgents = currentMissionSpecs.activeAgents
            changeIDHz = currentMissionSpecs.changeIDRates[agentIDint-1]
            sendHz = currentMissionSpecs.sendRates[agentIDint-1] # 10hz
            rcvHz = currentMissionSpecs.rcvRates[agentIDint-1]
            msgLength = currentMissionSpecs.msgLength
            startTime = currentMissionSpecs.trialStartTime #This needs to be added
            duration = currentMissionSpecs.experimentLength #This needs to be added

            endTime = startTime + duration
            #global currentRunningMax
            #2)  Set our send rate
            if sendHz>0.000001:
                sendRate = rospy.Rate(sendHz)
            spinRateS = rospy.Rate(100)
            sentMsgs = []
            sentBytes = []

            sendingMaxHistory = []
            msgNum = 0
            if time.time()>startTime:
                print "Too late...I missed the scheduled start time for Mission #", currentMissionID
                continue
            #3) Wait for start time  (add the randomizer here)
            print "Waiting for next mission to start"
            while time.time()<startTime:
                timeRemaining = startTime - time.time()
                #1currentRunningMax = 0
                # if abs(timeRemaining - round(timeRemaining))<0.02:
                #     print "t=", timeRemaining, currentRunningMax
                spinRateS.sleep()
            #currentRunningMax = 0
            delay = 1 + random.random()
            #print "Delay", '%0.4f' % delay, "s"
            rospy.sleep(delay)
            #4)  Mission will start now
            #print "S: Mission #:", currentMissionID, "sendRate: ", sendHz
            print "Mission Start, Send CM:", currentRunningMax
            if currentRunningMax > rcvHz:
                print "SOMETHING IS WRONG!!!!"
                print "CRM", currentRunningMax
            while time.time()<endTime:
                if sendHz<0.00001:
                    spinRateS.sleep()
                    #sendRate = rospy.Rate(1)
                else:
                    maxIDOutMsg = udpMessage.MaxID()
                    maxIDOutMsg.status = 0
                    maxIDOutMsg.missionID = currentMissionID
                    maxIDOutMsg.sender = agentIDint
                    maxIDOutMsg.time = time.time()
                    maxIDOutMsg.runningMax = currentRunningMax
                    maxIDOutMsg.msgLength = msgLength
                    maxIDOutMsg.msgNum = msgNum
                    cMaxAvgUDP = maxIDOutMsg.toUDPMessage()
                    sendingMaxHistory.append((time.time(),currentRunningMax))
                    #Send the msg
                    try:
                        sent = cMaxAvgUDP.sendOnUDP(sock,UDP_IP,UDP_PORT)
                        sentMsgs.append(maxIDOutMsg) #Record the Data
                        sentBytes.append(sent)
                    except socket.error:
                        #print "Couldn't send"
                        pass
                    msgNum +=1
                    sendRate.sleep()


            #5) Mission is Done, Save the Data and Clear Data
            sentResults = MissionSentResults()
            sentResults.missionID = currentMissionID
            sentResults.agentID = agentIDint
            sentResults.missionSpecs = currentMissionSpecs
            sentResults.sentMsgs = sentMsgs
            sentResults.sentBytes = sentBytes
            #sentFileName = experimentDirectory + '/sent' + str(agentIDint) + "SendHz" + str(sendHz) + "RcvdHz" + str(rcvHz) + 'UpdateHz'+ str(changeIDHz)+'MsgLength'+ str(msgLength) + "ID" + str(currentMissionID) + '.p'
            sentFileName = experimentDirectory + '/' + str(agentIDint) + 'sent' + '%02d' % currentMissionID + '.p'
            sentFile = open(sentFileName,'wb')
            pickle.dump(sentResults,sentFile)
            #print "Sent", len(sentMsgs), "Msgs In This Mission"
            #print "Mission End, Send CM:", sendingMaxHistory[-1][1]
            print "Saving ", len(sentMsgs), "SENT msgs to: ", sentFileName
            #6) This is likely redundent
            #print "Clearing Data"
            sentMsgs = []
            sentBytes = []
            msgNum = 0
        print "EXPERIMENT DONE!"
        print "Results Found:" + experimentDirectory

        global experimentRunning
        experimentRunning=False


def Receiving(entireMissionSpecs):
    neighbors = {
        1:{2,3},
        2:{1,3},
        3:{1,2,4,5},
        4:{1,2,3,5},
        5:{3,4,6},
        6:{4,5,6}
    }
    if simNeighbors:
        myNeighbors = neighbors[agentIDint]
    else:
        myNeighbors = {1,2,3,4,5,6} - {agentIDint}
    global currentRunningMax
    experimentRunning = True
    while experimentRunning and not rospy.is_shutdown():
        for currentMissionSpecs in entireMissionSpecs:
            #1)  Unpack the mission specs
            currentMissionID = currentMissionSpecs.missionID
            activeAgents = currentMissionSpecs.activeAgents
            rcvHz = currentMissionSpecs.rcvRates[agentIDint-1]
            msgLength = currentMissionSpecs.msgLength
            startTime = currentMissionSpecs.trialStartTime #This needs to be added
            duration = currentMissionSpecs.experimentLength #This needs to be added
            endTime = startTime + duration

            #2)  Set variables
            receiveRate = rospy.Rate(rcvHz)
            spinRateR = rospy.Rate(1000)
            rcvdMsgs = []
            rcvdBytes = []

            receivingMaxHistory = []
            currentMaxHistory = []

            currentRunningMax = 0
            if time.time()>startTime:
                print "RCVD: Too late...I missed the scheduled start time"
                continue

            #3) Wait for start time  (add the randomizer here)
            lastMessage = 0
            lastSocketError = 0
            while time.time()<startTime:
                timeRemaining = startTime-time.time()
                # if abs(timeRemaining - round(timeRemaining))<0.02:
                #     print "St=", timeRemaining, currentRunningMax
                try:
                    (data, addr) = sock.recvfrom(msgLength)
                    lastMessage = time.time()
                except socket.error:
                    lastSocketError = time.time()
                currentRunningMax = 0
                spinRateR.sleep()
            #print "Last MSG Rcvd", lastMessage
            #print "Last NO MSG Rcvd", lastSocketError
            currentRunningMax = 0
            receivedRunningMax = 0
            #print "R Specs: ", currentMissionID, "rcvRate (Hz)", rcvHz
            #print "Start Rcv CM:", currentRunningMax
            #print "Rcv RM:", receivedRunningMax
            #print "RCV CID", changingID
            #4)  Mission will start now
            if lastSocketError - lastMessage < 3:
                print "Warning: Last Clean Buffer vs. Rcvd=", lastSocketError - lastMessage, "s"
            while time.time()<endTime:
                try:
                    (data, addr) = sock.recvfrom(msgLength)
                    udp = udpMessage.udpMessage()
                    maxIDRcvMsg = udp.udpToInfoObject(data)
                    sender = maxIDRcvMsg.sender
                    if sender in myNeighbors: #Ignore if not in neighbors
                        maxIDRcvMsg.time = time.time() #This is the time I received the msg
                        receivedRunningMax = maxIDRcvMsg.runningMax
                        #print "R:", sender, ":", receivedRunningMax
                        maxIDRcvMsg.msgLength = sys.getsizeof(data)
                        currentRunningMax = max(currentRunningMax,receivedRunningMax,changingID)
                        receivingMaxHistory.append((time.time(),receivedRunningMax))
                        currentMaxHistory.append((time.time(),currentRunningMax))
                        rcvdMsgs.append(maxIDRcvMsg)
                except socket.error:
                    pass
                receiveRate.sleep()

            rcvdResults = MissionRcvdResults()
            rcvdResults.missionID = currentMissionID
            rcvdResults.agentID = agentIDint
            rcvdResults.missionSpecs = currentMissionSpecs
            rcvdResults.rcvdMsgs = rcvdMsgs

            #rcvdFileName = experimentDirectory + '/rcvd' + str(agentIDint) + "SendHz" + str(sendHz) + "RcvdHz" + str(rcvHz) + 'UpdateHz'+ str(changeIDHz)+'MsgLength'+ str(msgLength) + "ID" + str(currentMissionID) + '.p'

            rcvdFileName = experimentDirectory + '/' + str(agentIDint) + 'rcvd' + '%02d' % currentMissionID + '.p'
            rcvdFile = open(rcvdFileName,'wb')
            pickle.dump(rcvdResults,rcvdFile)  #TODO

            #print "Saving", len(rcvdMsgs), "RCVD msgs to: ", rcvdFileName
            #FLUSH DATA

            #print "END RCV", currentRunningMax
            currentRunningMax = 0
            #print "END RESET RCV", currentRunningMax

        experimentRunning = False





def IDChanging(entireMissionSpecs):
    experimentRunning = True
    while experimentRunning and not rospy.is_shutdown():
        for currentMissionSpecs in entireMissionSpecs:
            #1)  Unpack the mission specs
            currentMissionID = currentMissionSpecs.missionID
            activeAgents = currentMissionSpecs.activeAgents
            changeIDHz = currentMissionSpecs.changeIDRates[agentIDint-1]

            startTime = currentMissionSpecs.trialStartTime #This needs to be added
            duration = currentMissionSpecs.experimentLength #This needs to be added
            endTime = startTime + duration

            #1.changingID = agentIDint
            global changingID
            changingID = 0 #Starting from zero
            if changeIDHz<=0.00001:
                changeIDRate = rospy.Rate(50)
            else:
                changeIDRate = rospy.Rate(changeIDHz)

            changingMaxHistory = []
            #currentRunningMax = 0
            if time.time()>startTime:
                print "Too late...I missed the scheduled start time"
                continue

            #3) Wait for start time  (add the randomizer here)
            while time.time()<startTime:
                rospy.sleep(0.02)
            #1.currentRunningMax = 0
            #4)  Mission will start now
            #print "I Specs: ", currentMissionID, "Update Rate (Hz)", changeIDHz
            #print "ID: changingID", changingID
            while time.time()<endTime:
                if changeIDHz>0.00001:
                    changingID += 1
                    changingMaxHistory.append((time.time(),changingID))
                    # global currentRunningMax  #THIS IS THE ONLY VARIABLE SHARED BETWEEN THREADS
                    #1.currentRunningMax = max(currentRunningMax,changingID)
                    #print currentRunningMax
                    #currentRunningMaxHistory.append((rospy.get_time(),currentRunningMaxHistory))
                changeIDRate.sleep()
            #print "Final ChangingID:", changingID
            changingID = 0
            #print "Reset ChangingID:", changingID

        experimentRunning = False

def onShutdownSave():
    print "Shutting Down!"


if __name__ == '__main__':
    try:
        ##########################################
        #Global Variables
        agentIDstr=os.environ['AGENT_NAME'][-1]
        agentIDint = int(agentIDstr)
        agentInternalValue = agentIDint

        currentRunningMax = 0
        msgNum = 0
        bytesSent = 0
        experimentRunning = True
        missionRunning = False
        amountSent = 0
        newMission = True
        publishedRcvd = True
        publishedRcvd = True

        #Mission Spec Initialization
        currentMissionSpecs = None
        currentMissionID = -1
        activeAgents = [1,2,3,4,5,6]
        changeIDHz = 1
        sendHz = 20 # 10hz
        receiveHz = 10

        msgLength = 800
        experimentLength = 10


        #################################
        #Setting up the UDP Socket
        adhoc=True
        simNeighbors = False  #Set this true if you want to enforce neighbors
        if adhoc:
            UDP_IP = "10.10.3.255"
        else:
            UDP_IP = "128.31.39.255"
        UDP_PORT = 5005
        sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # UDP
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)  #Need this to allow broadcast
        sock.bind((UDP_IP,UDP_PORT))
        sock.setblocking(0) #Don't block
        print "UDP target IP:", UDP_IP, "port:", UDP_PORT
        ####################################
        # if len(sys.argv)>3:
        #     print "Please supply the filename"
        # else:
        arguments = rospy.myargv(argv=sys.argv)
        #resultsDirectory = '/home/pi/catkin_ws/src/adhoc_networking/results/'
        resultsDirectory = '/home/pi/results/'

        if len(arguments)<2:
            print "Please provide a missionSpecs File"
        else:
            experimentDirectory = resultsDirectory + arguments[1]
            maxIDNode(experimentDirectory)
    except rospy.ROSInterruptException:
        pass
