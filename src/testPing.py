#!/usr/bin/env python
import os, time, pickle
import subprocess
import numpy as np
startTime = time.time() + 1
n=10000
output = ""
#allSpeeds = [1,10,25,50,100,750,250,500,1000,1500]
allSpeeds = [25,100,200,250,1000]
#allSpeeds = [50,1000]
agentIDstr=os.environ['AGENT_NAME'][-1]
agentIDint = int(agentIDstr)
agentGroups = [{1},{2},{4},{1,2},{1,4}]
allSpeeds.sort(key=lambda x:x,reverse=True)
#allDelays = [0.10,0.075,0.05,.025,0.01,0.0075,.005,.001]
#allSpeeds = [.01]
pingresults = {}
nTrials = 3
delay = 2
maximumTimeDuration = 60
#print "Expected Time:", (len(agentGroups)*nTrials*(sum([min(maximumTimeDuration,n/speed)+delay for speed in allSpeeds])))/60, " mins"
print "Expected Time:", (len(agentGroups)*nTrials*(sum([20+delay for speed in allSpeeds])))/60, " mins"
for j in range(nTrials):
	for speed in allSpeeds:
		for agentGroup in agentGroups:
			while time.time()<startTime:
				#print "Waiting"
				pass
			#output += str(speed)
			print "trial ", j+1, "/", nTrials, " speed", speed
			totalTime = min(maximumTimeDuration,n/speed)
			totalTime = 20
			print "t total ", totalTime, "s"
			if agentIDint in agentGroup:
				print "I AM IN"
				results = subprocess.check_output("ping 10.10.3.3 -w "+str(totalTime)+ " -s 64 -i "+str(1.0/speed)+" | tail -2", shell=True)
				#print results
				parsed = results.split(' ')
				#print parsed
				rtt = parsed[12].split('/')
				nReceived = int(parsed[3])
				nTransmitted = int(parsed[0])
				pReceived = 1-int(parsed[5][:-1])/100.0
				meanRTT = float(rtt[1])
				mdevRTT = float(rtt[-1])
				if j==0:
					pingresults[speed] = []
				print "p", pReceived
				pingresults[speed] += [(agentGroup,nTransmitted,nReceived,pReceived,meanRTT,mdevRTT)]
				#print pingresults[speed]
				output += results
			else:
				print "Not in", agentGroup
			startTime += totalTime + delay
print output
filename = '/home/pi/results/'+str(agentIDint)+'allTogether64V1.p'
pickle.dump(pingresults,open(filename,'wb'))
print filename
#total = 0
#try:
#    ip_address = "10.10.3." + str(3)
#    response = os.system("ping " + ip_address + " -c 20 -s 1000 -i .01")
#    print response
#except KeyboardInterrupt:
#    print "Quitting from Keyboard"



    # for i in range(1,7):
    #     ip_address = "10.10.3." + str(i)
    #     print "Pinging", ip_address
    #     print response
    #     # t = 0
    #     # while t<5:
    #     #     t+=1
    #     #     if response == 1:
    #     #         print "Received Response from Agent" + str(i)
